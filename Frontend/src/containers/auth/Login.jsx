import React, { Component } from "react";
import { connect } from "react-redux";
import { Layout, Card, Col, Row, Form, Input, Button, Checkbox } from "antd";
import { Redirect, Link } from "react-router-dom";
import moment from "moment";

import * as authActions from "../../redux/actions/authActions";

import "./login.css";
import { UserOutlined, LockOutlined, LoginOutlined } from "@ant-design/icons";

const { Footer } = Layout;

class Login extends Component {
  state = {
    email: "",
    password: "",
    remember: true
  };

  componentDidMount() {
    if (this.props.authRedirectPath !== "/dashboard") {
      this.props.onSetAuthRedirectPath();
    }
    this.props.onTryAutoSignin();
  }

  onFinish = async (values) => {
    const { email, password } = values;
    await this.props.onAuth(email, password);
  };

  onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  render() {
    const { email, password, remember } = this.state;
    const { isAuthenticated, authRedirectPath, confirmLoading } = this.props;

    let authRedirect = null;
    if (isAuthenticated) {
      authRedirect = <Redirect to={authRedirectPath} />;
    }
    //  backgroundColor: "#f0f2f5"
    return (
      <Layout style={{ height: "100vh", backgroundColor: "#f0f2f5" }}>
        {authRedirect}
        <Row
          type="flex"
          justify="center"
          align="middle"
          style={{ height: "100vh" }}
        >
          <Col span={12} style={{ height: "100vh" }}>
            <Row type="flex" style={{ marginTop: 40 }}>
              <Col span={6}></Col>
              <Col type="flex" justify="center" align="middle" span={12}>
                <img
                  height="auto"
                  width="250px"
                  src="https://mikdantech.com/static/images/smartek21.png"
                  alt="Logo"
                  style={{ marginBottom: 20 }}
                />
              </Col>
              <Col span={6}></Col>
            </Row>
            <Row type="flex">
              <Col span={6}></Col>
              <Col span={12}>
                <Card id="login-form" title={null}>
                  <Form
                    style={{ userSelect: "none" }}
                    name="login"
                    className="login-form"
                    initialValues={{
                      remember,
                      email,
                      password,
                    }}
                    onFinish={this.onFinish}
                  >
                    <Form.Item
                      name="email"
                      rules={[{
                        type: 'email', message: 'Please input valid email.',
                      }, {
                        required: true, message: 'Please input your email.',
                      }
                      ]}
                    >
                      <Input
                        prefix={
                          <UserOutlined className="site-form-item-icon" />
                        }
                        type="email"
                        autoComplete={"email"}
                        placeholder="Email"
                      />
                    </Form.Item>
                    <Form.Item
                      name="password"
                      rules={[
                        {
                          required: true, message: "Please input your password."
                        },
                      ]}
                    >
                      <Input.Password
                        prefix={
                          <LockOutlined className="site-form-item-icon" />
                        }
                        autoComplete={"current-password"}
                        placeholder="Password"
                      />
                    </Form.Item>
                    <Form.Item>
                      <Form.Item
                        name="remember"
                        valuePropName="checked"
                        noStyle
                      >
                        <Checkbox>Remember me</Checkbox>
                      </Form.Item>
                      
                      <Link className="login-form-forgot" to="/login">
                        Forgot password
                      </Link>
                    </Form.Item>
                    <Form.Item>
                      <Button
                        icon={<LoginOutlined />}
                        loading={confirmLoading}
                        type="primary"
                        htmlType="submit"
                        className="login-form-button"
                      >
                        Log in
                      </Button>
                    </Form.Item>
                  </Form>
                </Card>
              </Col>
              <Col span={6}></Col>
            </Row>
            <Footer className="custom-footer" style={{ textAlign: "center" }}>
              <span>&copy; {moment().format("YYYY")} Mikdan Tech Solutions all rights reserved.</span>
            </Footer>
          </Col>
          <Col
            span={12}
            style={{
              height: "100vh",
              padding: 50,
              backgroundRepeat: "no-repeat",
              backgroundSize: "cover",
              backgroundPosition: 0,
              backgroundImage: `url("/salesforce_banner1.png")`,
            }}
          >
            <h1>World's #1 Mock Enterprise Application.</h1>
            <p style={{ color: "black", fontSize: 18, fontStyle: "italic" }}>
              Enterprise Applications (EA) are a software solution that provide business  <br /> logic and tools to model entire business processes for organisations to improve productivity and efficiency.
            </p>
          </Col>
        </Row>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.authReducer.loading,
    error: state.authReducer.error,
    isAuthenticated: state.authReducer.accessToken !== null,
    authRedirectPath: state.authReducer.authRedirectPath,
    confirmLoading: state.dataLoaderReducer.confirmLoading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAuth: (email, password) => dispatch(authActions.auth(email, password)),
    onSetAuthRedirectPath: () => dispatch(authActions.setAuthRedirectPath("/dashboard")),
    onTryAutoSignin: () => dispatch(authActions.authCheckState()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
