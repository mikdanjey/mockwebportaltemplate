import React, { Component } from "react";
import { Row, Col, Card } from "antd";
import { connect } from "react-redux";
import { Bar } from '@ant-design/charts';

class AdminDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {

    var data = [
      {
        year: '1951 年',
        value: 38,
      },
      {
        year: '1952 年',
        value: 52,
      },
      {
        year: '1956 年',
        value: 61,
      },
      {
        year: '1957 年',
        value: 145,
      },
      {
        year: '1958 年',
        value: 48,
      },
    ];
    var config = {
      data: data,
      xField: 'value',
      yField: 'year',
      seriesField: 'year',
      legend: { position: 'top-left' },
    };

    // const thisClass = this;
    // const { } = thisClass.state;
    return (
      <React.Fragment>
        <Row>
          <Col span={12}>
            <Card
              className="main-full-height"
              title="Dashboard 1"
            >

              <Bar {...config} />

            </Card>
          </Col>
          <Col span={12}>
            <Card
              className="main-full-height"
              title="Dashboard 2"
            >
            </Card>
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authReducer.accessToken !== null,
    accessToken: state.authReducer.accessToken,
    refreshToken: state.authReducer.refreshToken,
    userId: state.authReducer.userId,
    userFullName: state.authReducer.userFullName,
    userEmail: state.authReducer.userEmail,
    userRoles: state.authReducer.userRoles,
  };
};

export default connect(mapStateToProps)(AdminDashboard);
