import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Row, Col, Button, Table, Space, Drawer, Form, Input, Popconfirm } from "antd";
import axios from "axios";
import reqwest from 'reqwest';
import { PlusCircleOutlined, DeleteOutlined } from "@ant-design/icons";

import { AddProduct } from "./components/AddProduct";
import { UpdateProduct } from "./components/UpdateProduct";

import * as manageProductAction from "../../redux/actions/manageProductAction";
import * as customModalAction from "../../redux/actions/customModalAction";

import withErrorHandler from "../../hoc/withErrorHandler";


// const getRandomuserParams = params => {
//   return {
//     results: params.pagination.pageSize,
//     page: params.pagination.current,
//     ...params,
//   };
// };

class AdminManageProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      innerHeight: 0,
      currentRow: {},
      pagination: {
        current: 1,
        pageSize: 10,
      },
      visible: false,
    };
  }

  componentWillMount() {
    const { pagination } = this.state;
    this.props.readProduct(pagination);
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions = () => {
    const innerHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    this.setState({ innerHeight });
  }

  handleDataCreate = (values) => {
    this.props.createProduct(values);
  };

  handleDataUpdate = (values) => {
    this.props.updateProduct(values);
  };

  handleUpdateModal = (currentRow) => {
    new Promise((resolve, reject) => {
      this.setState({ currentRow });
      resolve(true);
    })
      .then((response) => {
        this.props.handleCustomModalVisible(false, true);
      });
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.fetch({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters,
    });
  };

  showDrawer = currentRow => {
    this.setState({
      visible: true,
      currentRow
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      // render: name => `${name.first} ${name.last}`,
      defaultSortOrder: 'ascend',
      sorter: (a, b) => { return a.name.localeCompare(b.name) },
      sortDirections: ['ascend', 'descend', 'canceld'],
      width: '20%',
    },
    {
      title: 'Price',
      dataIndex: 'price',
      sorter: (a, b) => a.price - b.price,
      width: '20%',
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
      sorter: (a, b) => a.quantity - b.quantity,
    },
    {
      title: 'Type',
      dataIndex: 'type',
      sorter: (a, b) => { return a.type.localeCompare(b.type) },
      sortDirections: ['ascend', 'descend', 'canceld'],
      filterMultiple: true,
      onFilter: (value, record) => record.type.indexOf(value) === 0,
      filters: [
        {
          text: 'Entertainment',
          value: 'Entertainment',
        },
        {
          text: 'Mobiles',
          value: 'Mobiles',
        },
        {
          text: 'Laptop',
          value: 'Laptop',
        },
        {
          text: 'Electronics',
          value: 'Electronics',
        },
        {
          text: 'Sports',
          value: 'Sports',
        },
        {
          text: 'Grocery',
          value: 'Grocery',
        },
        {
          text: 'Grooming',
          value: 'Grooming',
        },
        {
          text: 'Books',
          value: 'Books',
        },
        {
          text: 'Instruments',
          value: 'Instruments',
        },
        {
          text: 'Furniture',
          value: 'Furniture',
        },
        {
          text: 'Footwear',
          value: 'Footwear',
        },
        {
          text: 'Beauty',
          value: 'Beauty',
        },
      ],
    },
    {
      title: 'Action',
      sorter: false,
      dataIndex: 'id',
      key: "action",
      width: 350,
      render: (id, currentRow) => (
        <Space>
          <Popconfirm icon={<DeleteOutlined style={{ color: 'red' }} />} placement="topLeft" title={`Are you sure to delete id: ${id} product?`} onConfirm={() => this.props.deleteProduct({ id })} okText="Yes" cancelText="No">
            <Button type="link" danger={true}>Delete</Button>
          </Popconfirm>
          <Button type="link" onClick={() => this.handleUpdateModal(currentRow)}>Update</Button>
          <Button type="link" onClick={() => this.showDrawer(currentRow)}>Details</Button>
        </Space>
      )
    }
  ];

  fetch = (params = {}) => {
    reqwest({
      url: 'http://localhost:5001/products',
      method: 'get',
      type: 'json',
      // data: getRandomuserParams(params),
    }).then(data => {
      console.log(data);
      this.setState({
        data: data,
        pagination: {
          ...params.pagination,
          total: 10,
          // 200 is mock data, you should read it from server
          // total: data.totalCount,
        },
      });
    });
  };

  onChange = (pagination, filters, sorter, extra) => {
    console.log('params', pagination, filters, sorter, extra);
  }

  render() {
    const {
      visible,
      currentRow,
      innerHeight,
    } = this.state;

    const { products, confirmLoading, customModalVisible } = this.props;

    return (
      <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
        <Col span={24}>
          <Table
            style={{ userSelect: "text" }}
            title={() =>
              <Fragment>
                <Row>
                  <Col span={12}>
                    <h1>List of Products</h1>
                  </Col>
                  <Col span={12} style={{ textAlign: 'right' }}>
                    <Button icon={<PlusCircleOutlined />} type="primary" onClick={() => this.props.handleCustomModalVisible(true, false)}>Add Product</Button>
                  </Col>
                </Row>
              </Fragment>
            }
            columns={this.columns}
            scroll={{ y: innerHeight - 340 }}
            rowKey={record => record.id}
            dataSource={products}
            bordered={true}
            loading={confirmLoading}
            // pagination={pagination}
            // onChange={this.handleTableChange}
            onChange={this.onChange}
          />
          <AddProduct visible={customModalVisible.add} handleHideCustomModal={() => this.props.handleCustomModalVisible(false, false)} handleDataCreate={this.handleDataCreate} confirmLoading={confirmLoading} />

          <UpdateProduct visible={customModalVisible.update} handleHideCustomModal={() => this.props.handleCustomModalVisible(false, false)} handleDataUpdate={this.handleDataUpdate} currentRow={currentRow} confirmLoading={confirmLoading} />

          <Drawer
            title="Product Details"
            placement="right"
            width={500}
            closable={true}
            destroyOnClose={true}
            maskClosable={false}
            onClose={this.onClose}
            visible={visible}
            getContainer={true}
            bodyStyle={{ paddingBottom: 80 }}
            footer={
              <div
                style={{
                  textAlign: 'right',
                }}
              >
                <Button type="primary" onClick={this.onClose} style={{ marginRight: 8 }}>
                  Close
                </Button>
              </div>
            }
          >
            <Form
              layout="vertical"
              name="products"
              initialValues={{
                name: currentRow.name,
                price: currentRow.price,
                quantity: currentRow.quantity,
                type: currentRow.type,
                description: currentRow.description
              }}
            >
              <Form.Item
                name="name"
                label="Name"

                style={{ display: "inline-block", width: "calc(50% - 8px)" }}>
                <Input readOnly={true} />
              </Form.Item>

              <Form.Item
                name="price"
                label="Price"
                style={{
                  display: "inline-block",
                  width: "calc(50% - 8px)",
                  margin: "0 8px",
                }}
              >
                <Input readOnly={true} style={{ width: "100%" }} />
              </Form.Item>

              <Form.Item
                name="quantity"
                label="Quantity"
                style={{
                  display: "inline-block",
                  width: "calc(50% - 8px)",
                }}
              >
                <Input readOnly={true} style={{ width: "100%" }} />
              </Form.Item>

              <Form.Item
                name="type"
                label="Type"
                style={{
                  display: "inline-block",
                  width: "calc(50% - 8px)",
                  margin: "0 8px",
                }}
              >
                <Input readOnly={true} style={{ width: "100%" }} />
              </Form.Item>

              <Form.Item name="description" label="Description">
                <Input.TextArea readOnly={true} rows={5} />
              </Form.Item>
            </Form>
          </Drawer>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authReducer.accessToken !== null,
    accessToken: state.authReducer.accessToken,
    refreshToken: state.authReducer.refreshToken,
    userId: state.authReducer.userId,
    userFullName: state.authReducer.userFullName,
    userEmail: state.authReducer.userEmail,
    userRoles: state.authReducer.userRoles,
    products: state.manageProductReducer.products,
    confirmLoading: state.dataLoaderReducer.confirmLoading,
    customModalVisible: state.customModalReducer.customModalVisible,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createProduct: (product) => dispatch(manageProductAction.createProduct(product)),
    readProduct: (pagination) => dispatch(manageProductAction.readProduct(pagination)),
    updateProduct: (product) => dispatch(manageProductAction.updateProduct(product)),
    deleteProduct: (product) => dispatch(manageProductAction.deleteProduct(product)),
    handleCustomModalVisible: (add, update) => dispatch(customModalAction.customModalVisible(add, update)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(AdminManageProduct, axios));
