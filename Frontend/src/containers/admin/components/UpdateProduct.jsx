import React, { Component } from 'react';
import CustomModal from '../../../components/CustomModal';

import { Form, Input, InputNumber, Select } from "antd";
const { Option } = Select;

export class UpdateProduct extends Component {

    constructor(props) {
        super(props);

        this.state = {
        }
    }

    handleHideCustomModal = () => {
        this.props.handleHideCustomModal();
    };

    render() {
        const formId = "create-update-form"; // Import Id
        const { visible, confirmLoading, handleHideCustomModal, handleDataUpdate, currentRow } = this.props;
        return (
            <CustomModal title={`Product Update | id: ${currentRow.id}`} submitText={"Update"} visible={visible} handleHideCustomModal={handleHideCustomModal} formId={formId} confirmLoading={confirmLoading}>
                <Form
                    layout="vertical"
                    name="products"
                    initialValues={{
                        id: currentRow.id,
                        name: currentRow.name,
                        price: currentRow.price,
                        quantity: currentRow.quantity,
                        type: currentRow.type,
                        description: currentRow.description
                    }}
                    onFinish={handleDataUpdate}
                    id={formId}
                >

                    <Form.Item name="id" noStyle>
                        <Input type="hidden" />
                    </Form.Item>

                    <Form.Item
                        name="name"
                        label="Name"
                        rules={[
                            {
                                required: true,
                                message: "Please input name",
                            },
                        ]}
                        style={{ display: "inline-block", width: "calc(50% - 8px)" }}>
                        <Input />
                    </Form.Item>

                    <Form.Item
                        name="price"
                        label="Price"
                        rules={[
                            {
                                type: "number",
                                message: "The input must be numeric",
                            },
                            {
                                required: true,
                                message: "Please input price",
                            },
                        ]}
                        style={{
                            display: "inline-block",
                            width: "calc(50% - 8px)",
                            margin: "0 8px",
                        }}
                    >
                        <InputNumber style={{ width: "100%" }} />
                    </Form.Item>

                    <Form.Item
                        name="quantity"
                        label="Quantity"
                        rules={[
                            {
                                type: "number",
                                message: "The input must be numeric",
                            },
                            {
                                required: true,
                                message: "Please input quantity",
                            },
                        ]}
                        style={{
                            display: "inline-block",
                            width: "calc(50% - 8px)",
                        }}
                    >
                        <InputNumber style={{ width: "100%" }} />
                    </Form.Item>

                    <Form.Item
                        name="type"
                        label="Type"
                        rules={[{ required: true, message: "Please input type", }]}
                        style={{
                            display: "inline-block",
                            width: "calc(50% - 8px)",
                            margin: "0 8px",
                        }}>
                        <Select
                            allowClear={true}
                        >
                            <Option value="Entertainment">Entertainment</Option>
                            <Option value="Mobiles">Mobiles</Option>
                            <Option value="Laptop">Laptop</Option>
                            <Option value="Electronics">Electronics</Option>
                            <Option value="Sports">Sports</Option>
                            <Option value="Grocery">Grocery</Option>
                            <Option value="Grooming">Grooming</Option>
                            <Option value="Books">Books</Option>
                            <Option value="Instruments">Instruments</Option>
                            <Option value="Furniture">Furniture</Option>
                            <Option value="Footwear">Footwear</Option>
                            <Option value="Beauty">Beauty</Option>

                        </Select>
                    </Form.Item>

                    <Form.Item name="description" label="Description">
                        <Input.TextArea rows={3} />
                    </Form.Item>
                </Form>
            </CustomModal>
        );
    }
}