import React, { Component } from "react";
import { Row, Col, Steps, Card, Form, Input, Button, InputNumber, Select } from "antd";
import { connect } from "react-redux";

import { UserOutlined, SolutionOutlined, LoadingOutlined, SmileOutlined } from '@ant-design/icons';
const { Step } = Steps;
const { Option } = Select;


class AdminActivities extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  onFormLayoutChange = () => {

  };

  render() {
    // const thisClass = this;
    // const { } = this.state;
    return (
      <React.Fragment>

        <Row>
          <Col span={12}>
            <Card
              className="main-full-height"
              title="All Activities"
            >

              <Steps
                labelPlacement="vertical"
                direction="vertical"
                className="body-full-height"
                style={{ overflow: 'auto', }}
              >
                <Step status="finish" title="Login" description="This is a description." icon={<UserOutlined />} />
                <Step status="finish" title="Verification" description="This is a description." icon={<SolutionOutlined />} />
                <Step status="process" title="Pay" description="This is a description." icon={<LoadingOutlined />} />
                <Step status="wait" title="Done" description="This is a description." icon={<SmileOutlined />} />
                <Step status="finish" title="Login" description="This is a description." icon={<UserOutlined />} />
                <Step status="finish" title="Verification" description="This is a description." icon={<SolutionOutlined />} />
                <Step status="process" title="Pay" description="This is a description." icon={<LoadingOutlined />} />
                <Step status="wait" title="Done" description="This is a description." icon={<SmileOutlined />} />
                <Step status="finish" title="Login" description="This is a description." icon={<UserOutlined />} />
                <Step status="finish" title="Verification" description="This is a description." icon={<SolutionOutlined />} />
                <Step status="process" title="Pay" description="This is a description." icon={<LoadingOutlined />} />
                <Step status="wait" title="Done" description="This is a description." icon={<SmileOutlined />} />
                <Step status="finish" title="Login" description="This is a description." icon={<UserOutlined />} />
                <Step status="finish" title="Verification" description="This is a description." icon={<SolutionOutlined />} />
                <Step status="process" title="Pay" description="This is a description." icon={<LoadingOutlined />} />
                <Step status="wait" title="Done" description="This is a description." icon={<SmileOutlined />} />
                <Step status="finish" title="Login" description="This is a description." icon={<UserOutlined />} />
                <Step status="finish" title="Verification" description="This is a description." icon={<SolutionOutlined />} />
                <Step status="process" title="Pay" description="This is a description." icon={<LoadingOutlined />} />
                <Step status="wait" title="Done" description="This is a description." icon={<SmileOutlined />} />
                <Step status="finish" title="Login" description="This is a description." icon={<UserOutlined />} />
                <Step status="finish" title="Verification" description="This is a description." icon={<SolutionOutlined />} />
                <Step status="process" title="Pay" description="This is a description." icon={<LoadingOutlined />} />
                <Step status="wait" title="Done" description="This is a description." icon={<SmileOutlined />} />

                <Step status="finish" title="Login" description="This is a description." icon={<UserOutlined />} />
                <Step status="finish" title="Verification" description="This is a description." icon={<SolutionOutlined />} />
                <Step status="process" title="Pay" description="This is a description." icon={<LoadingOutlined />} />
                <Step status="wait" title="Done" description="This is a description." icon={<SmileOutlined />} />
                <Step status="finish" title="Login" description="This is a description." icon={<UserOutlined />} />
                <Step status="finish" title="Verification" description="This is a description." icon={<SolutionOutlined />} />
                <Step status="process" title="Pay" description="This is a description." icon={<LoadingOutlined />} />
                <Step status="wait" title="Done" description="This is a description." icon={<SmileOutlined />} />
                <Step status="finish" title="Login" description="This is a description." icon={<UserOutlined />} />
                <Step status="finish" title="Verification" description="This is a description." icon={<SolutionOutlined />} />
                <Step status="process" title="Pay" description="This is a description." icon={<LoadingOutlined />} />
                <Step status="wait" title="Done" description="This is a description." icon={<SmileOutlined />} />
                <Step status="finish" title="Login" description="This is a description." icon={<UserOutlined />} />
                <Step status="finish" title="Verification" description="This is a description." icon={<SolutionOutlined />} />
                <Step status="process" title="Pay" description="This is a description." icon={<LoadingOutlined />} />
                <Step status="wait" title="Done" description="This is a description." icon={<SmileOutlined />} />
                <Step status="finish" title="Login" description="This is a description." icon={<UserOutlined />} />
                <Step status="finish" title="Verification" description="This is a description." icon={<SolutionOutlined />} />
                <Step status="process" title="Pay" description="This is a description." icon={<LoadingOutlined />} />
                <Step status="wait" title="Done" description="This is a description." icon={<SmileOutlined />} />
                <Step status="finish" title="Login" description="This is a description." icon={<UserOutlined />} />
                <Step status="finish" title="Verification" description="This is a description." icon={<SolutionOutlined />} />
                <Step status="process" title="Pay" description="This is a description." icon={<LoadingOutlined />} />
                <Step status="wait" title="Done" description="This is a description." icon={<SmileOutlined />} />

              </Steps>

            </Card>

          </Col>

          <Col span={12}>

            <Card
              className="main-full-height"
              title={"Filter"}
            >

              <Form
                // form={form}
                layout="vertical"
                name="products"
                initialValues={{
                  name: "",
                  price: "",
                  quantity: "",
                  type: "",
                  description: ""
                }}
              // onFinish={handleDataCreate}
              // onValuesChange={onFormLayoutChange}
              // id={formId}
              >


                <Form.Item
                  name="name"
                  label="Name"
                  rules={[
                    {
                      required: true,
                      message: "Please input name",
                    },
                  ]}
                  style={{ display: "inline-block", width: "calc(50% - 8px)" }}>
                  <Input autoComplete={"off"} />
                </Form.Item>

                <Form.Item
                  name="price"
                  label="Price"
                  rules={[
                    {
                      type: "number",
                      message: "The input must be numeric",
                    },
                    {
                      required: true,
                      message: "Please input price",
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    margin: "0 8px",
                  }}
                >
                  <InputNumber style={{ width: "100%" }} />
                </Form.Item>

                <Form.Item
                  name="quantity"
                  label="Quantity"
                  rules={[
                    {
                      type: "number",
                      message: "The input must be numeric",
                    },
                    {
                      required: true,
                      message: "Please input quantity",
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                  }}
                >
                  <InputNumber style={{ width: "100%" }} />
                </Form.Item>

                <Form.Item
                  name="type"
                  label="Type"
                  rules={[{ required: true, message: "Please input type", }]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    margin: "0 8px",
                  }}>
                  <Select
                    allowClear={true}
                  >
                    <Option value="Clothing">Clothing</Option>
                    <Option value="Footwear">Footwear</Option>
                    <Option value="Jewelry">Jewelry</Option>
                    <Option value="Menswear">Menswear</Option>
                  </Select>
                </Form.Item>

                <Form.Item >
                  <br />
                  <Button type="primary">Filter</Button>
                </Form.Item>

              </Form>

            </Card>

          </Col>
        </Row>


      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authReducer.accessToken !== null,
    accessToken: state.authReducer.accessToken,
    refreshToken: state.authReducer.refreshToken,
    userId: state.authReducer.userId,
    userFullName: state.authReducer.userFullName,
    userEmail: state.authReducer.userEmail,
    userRoles: state.authReducer.userRoles,
  };
};

export default connect(mapStateToProps)(AdminActivities);
