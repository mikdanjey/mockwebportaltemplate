import React, { Component } from "react";

import { Layout, Menu } from "antd";

import { PieChartOutlined, UserOutlined } from "@ant-design/icons";

const { Sider } = Layout;
const { SubMenu } = Menu;

class AdminSideBar extends Component {
  render() {
    const { history } = this.props;
    let selectedKeys = history.location.pathname;
    if (selectedKeys === "/dashboard") {
      selectedKeys = "/";
    }
    const defaultOpenKeys = history.location.pathname.split("/")[1];
    return (
      <Sider
        collapsible
        collapsed={this.props.collapsed}
        onCollapse={this.props.onCollapse}
      >
        <div className="logo" style={{ paddingTop: 10, textAlign: "center" }}>
          <img
            style={{ width: "65%", height: "42px" }}
            src="https://mikdantech.com/static/images/smartek21.png"
            alt="Logo"
          />
        </div>

        <Menu
          theme="dark"
          selectedKeys={[selectedKeys]}
          defaultOpenKeys={[defaultOpenKeys]}
          mode="inline"
        >
          <Menu.Item
            key="/"
            icon={<PieChartOutlined />}
            onClick={() => history.push("/dashboard")}
          >
            Dashboard
          </Menu.Item>

          <Menu.Item
            key="/activities"
            icon={<PieChartOutlined />}
            onClick={() => history.push("/activities")}
          >
            Activities
          </Menu.Item>

          <SubMenu key="manage" icon={<UserOutlined />} title="Manage">
            <Menu.Item
              key="/manage/products"
              onClick={() => history.push("/manage/products")}
            >
              Products
            </Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>
    );
  }
}

export default AdminSideBar;
