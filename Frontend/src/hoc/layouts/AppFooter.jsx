import React from "react";
import moment from "moment";
import { Layout } from "antd";
const { Footer } = Layout;
const AppFooter = (props) => (
  <Footer
    style={{
      textAlign: "center",
      background: "white",
      color: "black",
    }}
  >
    <span>&copy; {moment().format("YYYY")} Mikdan Tech Solutions all rights reserved.</span>
  </Footer>
);

export default AppFooter;
