import React, { Component } from "react";
import { connect } from "react-redux";
import AppTopBar from "./AppTopBar";
import AppFooter from "./AppFooter";
import AppSideBar from "./AppSideBar";
import AppBreadcrumb from "./AppBreadcrumb";

import * as productActions from "../../redux/actions/manageProductAction";
import * as authActions from "../../redux/actions/authActions";

import { withRouter } from "react-router-dom";

import { Layout } from "antd";

const { Content } = Layout;

class AppLayout extends Component {
  state = {
    collapsed: false,
  };

  onCollapse = (collapsed) => {
    this.setState({ collapsed });
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  handleRoleChange = (currentRole) => {
    this.props.currentRoleChange(currentRole);
  };

  render() {
    const { collapsed } = this.state;
    const {
      isAuthenticated,
      userRoles,
      currentRole,
      history,
      userFullName,
    } = this.props;

    return (
      <React.Fragment>
        <Layout style={{ minHeight: "100vh" }} className="layout">
          <AppSideBar
            isAuthenticated={isAuthenticated}
            userRoles={userRoles}
            currentRole={currentRole}
            history={history}
            collapsed={collapsed}
            onCollapse={this.onCollapse}
            toggle={this.toggle}
          />

          <Layout className="site-layout">
            <AppTopBar
              history={history}
              userFullName={userFullName}
              collapsed={collapsed}
              toggle={this.toggle}
              userRoles={userRoles}
              currentRole={currentRole}
              handleRoleChange={this.handleRoleChange}
            />

            <Content style={{ margin: "0 16px" }}>
              <AppBreadcrumb history={history} />
                {this.props.children}
            </Content>
            <AppFooter />
          </Layout>
        </Layout>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authReducer.accessToken !== null,
    accessToken: state.authReducer.accessToken,
    refreshToken: state.authReducer.refreshToken,
    userId: state.authReducer.userId,
    userFullName: state.authReducer.userFullName,
    userEmail: state.authReducer.userEmail,
    userRoles: state.authReducer.userRoles,
    currentRole: state.authReducer.currentRole,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    readProduct: () => dispatch(productActions.readProduct()),
    currentRoleChange: (currentRole) =>
      dispatch(authActions.currentRoleChange(currentRole)),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(AppLayout)
);
