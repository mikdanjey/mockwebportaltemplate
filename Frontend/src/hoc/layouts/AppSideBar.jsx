import React, { Component } from "react";
import { Roles } from "../../AppConstants";

import AdminSideBar from "../../containers/admin/layouts/AdminSideBar";

class AppSideBar extends Component {
  render() {
    const {
      isAuthenticated,
      currentRole,
      history,
      collapsed,
      onCollapse,
    } = this.props;

    let sidebar = null;

    if (isAuthenticated && currentRole === Roles.Admin) {
      sidebar = (
        <AdminSideBar
          history={history}
          collapsed={collapsed}
          onCollapse={onCollapse}
        />
      );
    }

    return <React.Fragment>{sidebar}</React.Fragment>;
  }
}

export default AppSideBar;
