import React from "react";
import { withRouter } from "react-router-dom";
import { HomeFilled } from "@ant-design/icons";

import { Breadcrumb } from "antd";

const breadcrumbNameMap = {
  // Common
  "/dashboard": "Dashboard",
  "/activities": "Activities",

  // Admin
  "/manage": "Manage",
  "/manage/products": "Products",
};

const AppBreadcrumb = withRouter((props) => {
  const { location } = props;
  const pathSnippets = location.pathname.split("/").filter((i) => i);

  const extraBreadcrumbItems = pathSnippets.map((_, index) => {
    const url = `/${pathSnippets.slice(0, index + 1).join("/")}`;
    return (
      <Breadcrumb.Item key={url}>
        {/* <Link to={url}>{breadcrumbNameMap[url]}</Link> */}
        {breadcrumbNameMap[url]}
      </Breadcrumb.Item>
    );
  });

  const breadcrumbItems = [
    <Breadcrumb.Item key="Home">
      <HomeFilled />
    </Breadcrumb.Item>,
  ].concat(extraBreadcrumbItems);

  return (
    <Breadcrumb style={{ margin: "16px 0" }}>{breadcrumbItems}</Breadcrumb>
  );
});

export default AppBreadcrumb;
