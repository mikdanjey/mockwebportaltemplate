import React, { Component } from "react";

import {
  UserOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from "@ant-design/icons";

import { Layout, Menu, Dropdown, message } from "antd";
const { Header } = Layout;
const { ItemGroup } = Menu;

class AppTopBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentRole: props.currentRole,
    };
  }

  handleMenuClick = (e) => {
    switch (e.key) {
      case "2":
        message.info("Profile Feature Coming Soon.");
        break;
      case "3":
        this.props.history.push("/logout");
        break;
      default:
        break;
    }
  };

  handleRoleChange = (currentRole) => {
    new Promise((resolve, reject) => {
      this.props.handleRoleChange(currentRole);
      resolve(true);
    })
      .then((response) => {
        this.setState({ currentRole });
      })
      .then((response) => {
        this.props.history.push("/dashboard");
      });
  };

  render() {
    const { userFullName, userRoles } = this.props;
    const { currentRole } = this.state;
    const menu = (
      <Menu
        onClick={this.handleMenuClick}
        mode="vertical"
        selectedKeys={[currentRole]}
        theme={{
          theme: "dark",
          current: "1",
        }}
      >
        <ItemGroup
          title={
            <span>
              <UserOutlined />
              <span>Role Change</span>
            </span>
          }
        >
          {userRoles &&
            userRoles.map((item, index) => (
              <Menu.Item
                key={item}
                onClick={(item) => this.handleRoleChange(item.key)}
              >
                {item}
              </Menu.Item>
            ))}
        </ItemGroup>
        <Menu.Divider />
        <Menu.Item key="2" icon={<UserOutlined />}>
          Profile
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3" icon={<UserOutlined />}>
          Logout
        </Menu.Item>
      </Menu>
    );
    return (
      <Header style={{ paddingLeft: 40, backgroundColor: '#fff' }}>
        {React.createElement(
          this.props.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
          {
            className: "trigger",
            onClick: this.props.toggle,
          }
        )}

        <span style={{ float: "right" }}>
          <Dropdown.Button
            overlay={menu}
            theme={{
              theme: "dark",
              current: "1",
            }}
            icon={<UserOutlined />}
            placement="bottomLeft"
          >
            {currentRole !== null ? `${userFullName} as ${currentRole}` : `${userFullName} as No Roles`}
          </Dropdown.Button>
        </span>
      </Header>
    );
  }
}

export default AppTopBar;
