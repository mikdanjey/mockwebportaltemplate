import React from "react";
import { Helmet } from "react-helmet";

export const TitleComponent = ({ title }) => {
  var defaultTitle = "Web Portal Template";
  return (
    <Helmet>
      <title>{title ? `${title} - ${defaultTitle}` : defaultTitle}</title>
    </Helmet>
  );
};
