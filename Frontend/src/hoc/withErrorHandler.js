import React, { Component } from 'react';
import WrapUp from './WrapUp';


const withErrorHandler = (WrappedComponent, axios) => {
    return class extends Component {
        state = {
            error: null
        }

        componentWillMount() {
            this.reqInterceptor = axios.interceptors.request.use(req => {
                this.setState({ error: null });
                return req;
            });
            this.resInterceptor = axios.interceptors.response.use(res => {
                return res;
            },
                error => {
                    let allError = [];
                    allError.push(error.response.data.email.join());
                    allError.push(error.response.data.employee_id.join());
                    this.setState({ error: allError.join(" ")});
                }
            );
        }

        componentWillUnmount() {
            axios.interceptors.request.eject(this.reqInterceptor);
            axios.interceptors.response.eject(this.resInterceptor);
        }

        render() {
            // console.log('withError', this.state);
            return (
                <WrapUp>
                    {/* <Fragment>
                        {this.state.error ? toastr.error(this.state.error) : null}
                    </Fragment> */}
                    <WrappedComponent {...this.props} />
                </WrapUp>
            );
        }
    }
}

export default withErrorHandler;