import React from "react";
import "./page_loading.css";

export const PageLoading = () => {
    return <div className="page-loader" />
}
