import React, { Component } from "react";
// import { lazy } from "react";
import { Roles } from "./AppConstants";
import { Route, Switch, withRouter } from "react-router-dom";

import { connect } from "react-redux";
import * as actions from "./redux/actions/authActions";

import { withPageTitle } from "./helpers/withPageTitle";
import PageNotFound from "./containers/PageNotFound";

// Common Modules
import AppLayout from "./hoc/layouts/AppLayout";
import Logout from "./containers/auth/Logout";
import Login from "./containers/auth/Login";

// Admin Modules
import AdminDashboard from "./containers/admin/AdminDashboard";
import AdminActivities from "./containers/admin/AdminActivities";
import AdminManageProduct from "./containers/admin/AdminManageProduct";

// const timeOut = 100;

// const AdminDashboard = lazy(() => {
//   return new Promise(resolve => {
//     setTimeout(() => resolve(import("./containers/admin/AdminDashboard")), timeOut);
//   });
// });

// const AdminActivities = lazy(() => {
//   return new Promise(resolve => {
//     setTimeout(() => resolve(import("./containers/admin/AdminActivities")), timeOut);
//   });
// });

// const AdminManageProduct = lazy(() => {
//   return new Promise(resolve => {
//     setTimeout(() => resolve(import("./containers/admin/AdminManageProduct")), timeOut);
//   });
// });

class PrivateRoutes extends Component {

  componentDidMount() {
    this.props.onTryAutoSignin();
  }

  render() {
    const { isAuthenticated, currentRole } = this.props;
    let routes = null;

    if (isAuthenticated && currentRole === Roles.Admin) {
      routes = (
        <Switch>
          <Route exact path="/logout" component={Logout} />
          <Route
            path="/"
            exact
            component={withPageTitle({ component: AdminDashboard, title: 'Dashboard' })}
          />
          <Route
            path="/dashboard"
            exact
            component={withPageTitle({ component: AdminDashboard, title: 'Dashboard' })}
          />
          <Route
            path="/activities"
            exact
            component={withPageTitle({ component: AdminActivities, title: 'Activities' })}
          />
          <Route
            path="/manage/products"
            exact
            component={withPageTitle({ component: AdminManageProduct, title: 'Manage Products' })}
          />
          <Route
            path='*'
            exact={true}
            component={withPageTitle({ component: PageNotFound, title: '404' })}
          />
        </Switch>
      );
    }
    else {
      routes = (
        <Switch>
          <Route exact path="/logout" component={Logout} />
          <Route
            path="/login"
            exact
            component={withPageTitle({ component: Login, title: 'Login' })}
          />
          <Route
            path="/"
            exact
            component={withPageTitle({ component: Login, title: 'Login' })}
          />
          <Route
            path='*'
            exact={true}
            component={withPageTitle({ component: PageNotFound, title: '404' })}
          />
        </Switch>
      );
    }

    if (isAuthenticated) {
      return (<AppLayout>
        {routes}
      </AppLayout>);
    } else {
      return <Login />;
    }
  }

}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.authReducer.accessToken !== null,
    accessToken: state.authReducer.accessToken,
    refreshToken: state.authReducer.refreshToken,
    userId: state.authReducer.userId,
    userFullName: state.authReducer.userFullName,
    userEmail: state.authReducer.userEmail,
    currentRole: state.authReducer.currentRole
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignin: () => dispatch(actions.authCheckState())
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PrivateRoutes));