//Login
export const userLogin = "/users/_login";
export const userForgotPassword = "/users/forgot_password";
export const userResetPassword = "/users/confirm_password";
export const temporaryPassword = "/users/_changetemppassword";

//Categories
export const categoriesList = "/categories/_list";
export const categoriesCreate = "/categories/_create";