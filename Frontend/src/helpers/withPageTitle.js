
import React, { Component as BaseComponent } from "react";

import { TitleComponent } from "../hoc/TitleComponent";

export const withPageTitle = ({ component: Component, title }) => {
    return class Title extends BaseComponent {
        render() {
            return (
                <React.Fragment>
                    <TitleComponent title={title} />
                    <Component {...this.props} />
                </React.Fragment>
            );
        }
    };
};