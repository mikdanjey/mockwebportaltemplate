// Imports: Dependencies
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

// Imports: Redux
import rootReducer from '../reducers';

// Custom Options
const logger = createLogger({
    duration: false,
    timestamp: false,
    predicate: () => process.env.REACT_APP_ENV !== "production"
});

// const composeEnhancers = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Redux: Store
const store =
    createStore(
        rootReducer,
        composeEnhancers(
            applyMiddleware(thunk, logger)
        )
    );

// Exports
export {
    store
};