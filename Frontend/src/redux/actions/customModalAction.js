import { DATA_MODAL } from "../constants";

const customModalVisible = (add, update) => {
  return {
    type: DATA_MODAL,
    customModalVisible: {
      add,
      update
    }
  };
};

export { customModalVisible };
