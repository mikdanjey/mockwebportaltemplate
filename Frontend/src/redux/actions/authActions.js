import axios from "axios";
import { AUTH } from "../constants";
import { confirmLoading } from "./dataLoaderAction";
const currentRoleChange = (currentRole) => {
  localStorage.setItem("currentRole", currentRole);
  return {
    type: AUTH.ROLE_CHANGE,
    currentRole: currentRole
  };
};

const authStart = () => {
  return {
    type: AUTH.START
  };
};

const authSuccess = (
  accessToken,
  refreshToken,
  userId,
  userFullName,
  userEmail,
  userRoles,
  currentRole
) => {
  return {
    type: AUTH.SUCCESS,
    accessToken,
    refreshToken,
    userId,
    userFullName,
    userEmail,
    userRoles,
    currentRole
  };
};

const authFail = error => {
  return {
    type: AUTH.FAIL,
    error: error
  };
};

const logout = () => {
  localStorage.removeItem("accessToken");
  localStorage.removeItem("refreshToken");
  localStorage.removeItem("userId");
  localStorage.removeItem("userFullName");
  localStorage.removeItem("userEmail");
  localStorage.removeItem("userRoles");
  localStorage.removeItem("currentRole");
  return {
    type: AUTH.LOGOUT
  };
};

// const checkAuthTimeout = expirationTime => {
//   return (dispatch, getState) => {
//     setTimeout(() => {
//       dispatch(logout());
//     }, expirationTime * 1000);
//   };
// };

const makeDummyRequest = async (email, password) => { // Sample async
  const config = {
    method: 'POST',
    url: "/api/v1/token",
    data: {
      email,
      password
    },
    headers: {
      "Content-Type": "application/json"
    }
  }

  let response = await axios(config);

  console.log(response.data);
}

const auth = (email, password) => {
  return (dispatch, getState) => {

    dispatch(confirmLoading(true));
    dispatch(authStart());

    // Headers
    const config = {
      headers: {
        "Content-Type": "application/json"
      }
    };

    const body = {
      email: email,
      password: password
    };

    let url = "/api/v1/token";

    axios
      .post(url, body, config)
      .then(response => {
        // console.log("response", response);
        // const expirationDate = new Date(new Date().getTime() + response.data.expiresIn * 1000);

        localStorage.setItem("accessToken", response.data.access);
        localStorage.setItem("refreshToken", response.data.refresh);
        localStorage.setItem("userId", response.data.id);
        localStorage.setItem("userFullName", response.data.full_name);
        localStorage.setItem("userEmail", response.data.email);
        localStorage.setItem("userRoles", response.data.roles);

        let userRoles = localStorage.getItem("userRoles");
        let currentRole = null;
        if (userRoles !== null && userRoles.trim().length !== 0) {
          userRoles = userRoles.split(",");
          localStorage.setItem("currentRole", userRoles[0]);
          currentRole = userRoles[0];
        } else {
          localStorage.setItem("currentRole", null);
          currentRole = null;
        }
        setTimeout(() => {
          dispatch(confirmLoading(false));
          dispatch(
            authSuccess(
              response.data.access,
              response.data.refresh,
              response.data.id,
              response.data.full_name,
              response.data.email,
              response.data.roles,
              currentRole
            )
          );
        }, 300);
        // dispatch(checkAuthTimeout(response.data.expiresIn));
      })
      .catch(err => {
        console.log("login err", err);
        dispatch(authFail(err.response.data.error));
      });
  };
};

const setAuthRedirectPath = path => {
  return {
    type: AUTH.REDIRECT_PATH,
    path: path
  };
};

const authCheckState = () => {
  return (dispatch, getState) => {
    const accessToken = localStorage.getItem("accessToken");

    if (!accessToken) {
      dispatch(logout());
    } else {
      const refreshToken = localStorage.getItem("refreshToken");
      const userId = localStorage.getItem("userId");
      const userFullName = localStorage.getItem("userFullName");
      const userEmail = localStorage.getItem("userEmail");
      const currentRole = localStorage.getItem("currentRole");
      let userRoles = localStorage.getItem("userRoles");

      if (userRoles !== null && userRoles.trim().length !== 0) {
        userRoles = userRoles.split(",");
      }

      dispatch(
        authSuccess(
          accessToken,
          refreshToken,
          userId,
          userFullName,
          userEmail,
          userRoles,
          currentRole
        )
      );
      // dispatch(
      //   checkAuthTimeout(
      //     (expirationDate.getTime() - new Date().getTime()) / 1000
      //   )
      // );
    }
  };
};

// Setup config with token - helper function
const accessTokenConfig = getState => {
  // Get token from state
  // const accessToken = getState().authReducer.accessToken;
  const accessToken = localStorage.getItem("accessToken");

  // Headers
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  // If token, add to headers config
  if (accessToken) {
    config.headers["Authorization"] = `Bearer ${accessToken}`;
  }

  return config;
};

export { auth, logout, setAuthRedirectPath, authCheckState, accessTokenConfig, currentRoleChange, makeDummyRequest, };
