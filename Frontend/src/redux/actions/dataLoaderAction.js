import { DATA_LOADER } from "../constants";

const confirmLoading = isConfirmLoading => {
  return {
    type: DATA_LOADER,
    confirmLoading: isConfirmLoading
  };
};

export { confirmLoading };
