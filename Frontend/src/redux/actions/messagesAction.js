import { MESSAGE } from "../constants";
import { notification } from 'antd';

// success Message
const successMessage = (msg, status) => {

  notification.info({
    message: 'Success',
    duration: 1,
    description: msg,
    onClick: () => {
      console.log('Notification Clicked!');
    },
  });

  console.log('mani');

  return {
    type: MESSAGE.CREATE,
    messages: { msg, status }
  };
};

// error Message
const errorMessage = (msg, status) => {
  notification.error({
    message: 'Failed',
    duration: 3,
    description: msg,
    onClick: () => {
      console.log('Notification Clicked!');
    },
  });
  return {
    type: MESSAGE.ERRORS,
    messages: { msg: msg, status }
  };
};

export { successMessage, errorMessage };
