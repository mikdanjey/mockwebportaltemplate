import axios from "axios";
import { MANAGE_PRODUCT_TYPE } from "../constants";
import { accessTokenConfig } from "./authActions";

import { successMessage, errorMessage } from "./messagesAction";

import { confirmLoading } from "./dataLoaderAction";

import { customModalVisible } from "./customModalAction";

const createProduct = product => {
  return (dispatch, getState) => {
    dispatch(confirmLoading(true));
    axios
      .post("http://localhost:5001/products", product, accessTokenConfig(getState))
      .then(res => {
        if (res !== undefined && res !== null) {
          console.log('create Product res', res);
          dispatch({
            type: MANAGE_PRODUCT_TYPE.CREATE_DATA,
            createResponse: res.data,
          });
          setTimeout(() => {
            dispatch(confirmLoading(false));
            dispatch(customModalVisible(false, false));
            dispatch(readProduct());
            dispatch(successMessage('Product Added.', res.status));
          }, 1000);
        }
      })
      .catch(err => {
        console.log('Product Added err:', err);
        dispatch(errorMessage('Data not created', 400)); // err.response, err.response.status
      });
  };
};

// READ Product
const readProduct = (pagination) => {
  return (dispatch, getState) => {
    dispatch(confirmLoading(true));
    axios
      .get("http://localhost:5001/products", accessTokenConfig(getState))
      .then(res => {
        dispatch({
          type: MANAGE_PRODUCT_TYPE.READ_DATA,
          products: res.data
        });
        dispatch(confirmLoading(false));
        // dispatch(successMessage('Data Loaded.', res.status));
      })
      .catch(err => {
        console.log('Product Read err:', err);
        dispatch(errorMessage(err.response.data, err.response.status));
      });
  };
};

const updateProduct = product => {
  return (dispatch, getState) => {
    dispatch(confirmLoading(true));
    axios
      .put("http://localhost:5001/products/" + product.id, product, accessTokenConfig(getState))
      .then(res => {
        if (res !== undefined && res !== null) {
          console.log('update Product res', res);
          dispatch({
            type: MANAGE_PRODUCT_TYPE.UPDATE_DATA,
            updateResponse: res.data
          });
          setTimeout(() => {
            dispatch(confirmLoading(false));
            dispatch(customModalVisible(false, false));
            dispatch(readProduct());
            dispatch(successMessage('Product Updated.', res.status));
          }, 1000);
        }
      })
      .catch(err => {
        console.log('Product Update err:', err);
        dispatch(errorMessage('Product not Updated.', 400)); // err.response, err.response.status
      });
  };
};

const deleteProduct = product => {
  return (dispatch, getState) => {
    axios
      .delete("http://localhost:5001/products/" + product.id, accessTokenConfig(getState))
      .then(res => {
        if (res !== undefined && res !== null) {
          console.log('delete Product res', res);
          dispatch({
            type: MANAGE_PRODUCT_TYPE.DELETE_DATA,
            deleteResponse: res.data
          });
          dispatch(successMessage('Product Deleted.', res.status));
          dispatch(readProduct());
        }
      })
      .catch(err => {
        console.log('Product Deleted err:', err);
        dispatch(errorMessage('Product not Deleted.', 400)); // err.response, err.response.status
      });
  };
};

export { createProduct, readProduct, updateProduct, deleteProduct };
