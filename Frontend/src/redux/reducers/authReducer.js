import { AUTH } from '../constants';

import { updateObject } from '../util/ReduxUtil';

// Initial State
const initialState = {
  accessToken: null,
  refreshToken: null,
  userId: null,
  userFullName: null,
  userEmail: null,
  userRoles: [],
  currentRole: null,
  error: null,
  loading: false,
  isAuthenticated: false,
  authRedirectPath: "/dashboard"
};

const authStart = (state, action) => {
  return updateObject(state, { error: null, loading: true });
};

const authSuccess = (state, action) => {
  return updateObject(state, {
    accessToken: action.accessToken,
    refreshToken: action.refreshToken,
    userId: action.userId,
    userFullName: action.userFullName,
    userEmail: action.userEmail,
    userRoles: action.userRoles,
    currentRole: action.currentRole,
    error: null,
    loading: false
  });
};

const authFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const authLogout = (state, action) => {
  return updateObject(state, { accessToken: null, userId: null });
};

const setAuthRedirectPath = (state, action) => {
  return updateObject(state, { authRedirectPath: action.path })
}

// Reducers (Modifies The State And Returns A New State)
const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH.START: return authStart(state, action);
    case AUTH.SUCCESS: return authSuccess(state, action);
    case AUTH.FAIL: return authFail(state, action);
    case AUTH.LOGOUT: return authLogout(state, action);
    case AUTH.REDIRECT_PATH: return setAuthRedirectPath(state, action);
    case AUTH.ROLE_CHANGE:
      return {
        ...state,
        currentRole: action.currentRole,
      };
    default:
      return state;
  }
};

// Exports
export default authReducer;