import { MANAGE_PRODUCT_TYPE } from "../constants";

const initialState = {
  createResponse: {},
  products: [],
  updateResponse: {},
  deleteResponse: {}
};

const manageProductReducer = (state = initialState, action) => {
  switch (action.type) {

    case MANAGE_PRODUCT_TYPE.CREATE_DATA:
      return {
        ...state,
        createResponse: action.createResponse,
      };

    case MANAGE_PRODUCT_TYPE.READ_DATA:
      return {
        ...state,
        products: action.products
      };

    case MANAGE_PRODUCT_TYPE.UPDATE_DATA:
      return {
        ...state,
        updateResponse: action.updateResponse
      };

    case MANAGE_PRODUCT_TYPE.DELETE_DATA:
      return {
        ...state,
        deleteResponse: action.deleteResponse
      };

    default:
      return state;
  }
};

export default manageProductReducer;
