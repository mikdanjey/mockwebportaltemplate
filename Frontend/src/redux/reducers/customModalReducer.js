
import { DATA_MODAL } from "../constants";

const initialState = {
    customModalVisible: {
        add: false,
        update: false
    },
};

const customModalReducer = (state = initialState, action) => {
    switch (action.type) {
        case DATA_MODAL:
            return {
                ...state,
                customModalVisible: action.customModalVisible
            };
        default:
            return state;
    }
}

export default customModalReducer;