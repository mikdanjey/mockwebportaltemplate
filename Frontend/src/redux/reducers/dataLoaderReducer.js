
import { DATA_LOADER } from "../constants";

const initialState = {
    confirmLoading: false,
};

const dataLoaderReducer = (state = initialState, action) => {
    switch (action.type) {
        case DATA_LOADER:
            return {
                ...state,
                confirmLoading: action.confirmLoading
            };
        default:
            return state;
    }
}

export default dataLoaderReducer;