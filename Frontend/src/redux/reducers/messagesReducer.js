
import { MESSAGE } from "../constants";

const initialState = {};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case MESSAGE.CREATE:
            return {
                ...state,
                messages: action.messages
            };
        case MESSAGE.ERRORS:
            return {
                ...state,
                messages: action.messages
            };
        default:
            return state;
    }
}

export default messagesReducer;