// Imports: Dependencies
import { combineReducers } from 'redux';

// Imports: Reducers
import authReducer from './authReducer';
import messagesReducer from './messagesReducer';

import dataLoaderReducer from './dataLoaderReducer';
import customModalReducer from './customModalReducer';

import manageProductReducer from './manageProductReducer';

const reducers = {
  authReducer,
  messagesReducer,
  dataLoaderReducer,
  customModalReducer,
  manageProductReducer,
}

// Redux: Root Reducer
const rootReducer = combineReducers(reducers);

// Exports
export default rootReducer;