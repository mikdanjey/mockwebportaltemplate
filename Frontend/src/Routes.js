import React from "react";
import { Switch } from "react-router";
import { Route, withRouter } from "react-router-dom";
import { withPageTitle } from "./helpers/withPageTitle";
import asyncComponent from "./hoc/asyncComponent";
import AuthGuard from "./AuthGuard";
// import PrivateRoutes from "./PrivateRoutes";
// import Login from "./containers/auth/Login";

const Login = asyncComponent(() => {
    return import("./containers/auth/Login");
});

const PrivateRoutes = asyncComponent(() => {
    return import("./PrivateRoutes");
});

const Routes = ({ history }) => (
    <Switch>
        <Route
            path="/login"
            exact
            component={withPageTitle({ component: Login, title: 'Login' })}
        />
        <AuthGuard exact component={PrivateRoutes} />
    </Switch>
);

export default withRouter(Routes);
