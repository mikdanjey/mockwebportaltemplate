import axios from "axios";
import { userURL } from "./helpers/config";

const fetchClient = () => {
    const defaultOptions = {
        headers: {
            "Content-Type": "application/json",
        },
    };

    // Create instance
    let instance = axios.create(defaultOptions);

    // Set the AUTH token for any request
    instance.interceptors.request.use(function (config) {
        config.headers.Authorization = localStorage.getItem("identityToken").slice(1, -1);
        config.headers.access_token = localStorage.getItem("accessToken");
        config.headers.identity_token = localStorage.getItem("identityToken");
        return config;
    });

    instance.interceptors.response.use(
        (response) => {
            return response;
        },
        (error) => {
            // Below code will Refresh token if the existing token expires in any of the API called
            if (error.response && error.response.status === 401) {
                let refreshURL = `${userURL}/users/_refreshtoken`;
                axios
                    .get(refreshURL, {
                        headers: {
                            "Content-Type": "application/json",
                            access_token: localStorage.getItem("accessToken"),
                            identity_token: localStorage.getItem("identityToken")
                        },
                    })
                    .then((response) => {
                        localStorage.setItem("accessToken", JSON.stringify(response.data.access_token));
                        localStorage.setItem("identityToken", JSON.stringify(response.data.identity_token));
                    })
                    .catch((error) => {
                        if (localStorage.getItem("isShowToast") !== null && (localStorage.getItem("isShowToast") === "false")) {
                            // toast.error("Session Expired. Redirecting to login page");
                            localStorage.setItem("isShowToast", true);
                        } else {
                            localStorage.setItem("isShowToast", false);
                        }
                        setTimeout(() => {
                            localStorage.clear();
                            return (window.location.href = "/login");
                        }, 1000);
                    });
            }
            return Promise.reject(error);
        },
    );

    return instance;
};

export default fetchClient();
