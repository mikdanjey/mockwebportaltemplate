import React from 'react';
import { Skeleton} from "antd";

const CustomSkeleton = (props) => {
    const { isActive } = props;
    return (
        <Skeleton paragraph={{ rows: 10 }} active={isActive} title={isActive} round={isActive} loading={isActive} />
    )
}

export default CustomSkeleton;