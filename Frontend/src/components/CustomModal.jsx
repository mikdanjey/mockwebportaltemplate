import React from 'react';
import { Modal, Button } from "antd";

const CustomModal = (props) => {
    const { formId, title, submitText, visible, confirmLoading, handleHideCustomModal } = props;
    return (
        <Modal
            maskClosable={false}
            title={title}
            visible={visible}
            onCancel={handleHideCustomModal}
            destroyOnClose={true}
            style={{ userSelect: "none" }}
            footer={[
                <Button
                    key="button"
                    onClick={handleHideCustomModal}
                    htmlType="button">Cancel</Button>,
                <Button
                    form={formId}
                    key="submit"
                    htmlType="submit"
                    type="primary"
                    loading={confirmLoading}>{submitText}</Button>,
            ]}>
            {props.children}
        </Modal>
    )
}

export default CustomModal;