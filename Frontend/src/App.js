import React, { Component, Suspense } from "react";
import "./App.css";
import { BrowserRouter } from "react-router-dom";

import Routes from "./Routes";
import { PageLoading } from './PageLoading';

import { Provider as StoreProvider } from 'react-redux';

import { store } from './redux/store';

import axios from 'axios';
import { baseURL } from "./helpers/config";
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

history.listen((location, action) => {
  console.log(location);
});

axios.defaults.baseURL = baseURL;
axios.interceptors.response.use((response) => {
    // console.log("axios.interceptors.response", response);
    return response;
}, async error => {
    // console.log("axios.interceptors.error", error);
    // handle the response error
    if (error.response.status !== 401) {
        return new Promise((resolve, reject) => {
            reject(error);
        });
    } else {
        try {
            await getRefreshToken(error);

            const accessToken = localStorage.getItem('accessToken');
            const config = error.config;
            config.headers['Authorization'] = "Bearer " + accessToken;
            return new Promise((resolve, reject) => {
                axios.request(config).then(response => {
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        }
        catch (error_1) {
            Promise.reject(error_1);
        }
    }
});

function getRefreshToken(error) {
    return new Promise((resolve, reject) => {
        const refreshToken = localStorage.getItem('refreshToken');
        try {
            if (refreshToken != null) {

                const url = "/api/v1/token/refresh";
                const config = {
                    headers: {
                        "Content-Type": "application/json"
                    }
                };
                const body = {
                    refresh: refreshToken
                };
                axios
                    .post(url, body, config)
                    .then(response => {
                        const accessToken = response.data.access;
                        console.log('accessToken from refreshToken', accessToken);
                        localStorage.setItem("accessToken", accessToken);
                    })
                    .catch(err => {
                        console.log('Refresh Token Error', err);
                    });

            } else {
            //   toastr.error("Invalid Credential");
            }
        } catch (err) {
        }

    });
}

class App extends Component {
  // componentDidMount() {
  //   let waitTime = 15 * 60 * 1000;
  //   this.refreshTokenTimer = setInterval(() => {
  //     let refreshURL = "/api/v1/token/refresh/";
  //     axios
  //       .get(refreshURL, {
  //         headers: {
  //           "Content-Type": "application/json",
  //           access_token: localStorage.getItem("accessToken"),
  //           identity_token: localStorage.getItem("identityToken")
  //         },
  //       })
  //       .then((response) => {
  //         localStorage.setItem("accessToken", JSON.stringify(response.data.access_token));
  //         localStorage.setItem("identityToken", JSON.stringify(response.data.identity_token));
  //       })
  //       .catch((error) => {
  //         console.log(error);
  //       });
  //   }, waitTime);
  // }

  // componentWillUnmount() {
  //   clearInterval(this.refreshTokenTimer);
  // }

  render() {
    return (
      <StoreProvider store={store}>
        <Suspense fallback={<PageLoading />}>
          <BrowserRouter>
            <Routes />
          </BrowserRouter>
        </Suspense>
      </StoreProvider>
    );
  }
}

export default App;
